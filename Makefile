GO_FILES=`go list ./... | grep -v -E "mock|store|test|fake|cmd|bin|backend|google|logger|proto"`

proto-build:
	protoc -I=. --go_out=:./proto/go/ --go_opt=paths=source_relative \
    --go-grpc_out=:./proto/go/ --go-grpc_opt=paths=source_relative ./backend/common/*.proto

	protoc -I=. --go_out=:./proto/go/ --go_opt=paths=source_relative \
    --go-grpc_out=:./proto/go/ --go-grpc_opt=paths=source_relative ./backend/pasture/*.proto

	protoc -I=. --go_out=:./proto/go/ --go_opt=paths=source_relative \
    --go-grpc_out=:./proto/go/ --go-grpc_opt=paths=source_relative ./backend/operation/*.proto

proto-build-xdmy:
	protoc -I=. --go_out=:./proto/go/ --go_opt=paths=source_relative \
    --go-grpc_out=:./proto/go/ --go-grpc_opt=paths=source_relative ./backend/xdmy/*.proto

proto-build-event:
	protoc -I=. --go_out=:./proto/go/ --go_opt=paths=source_relative \
    --go-grpc_out=:./proto/go/ --go-grpc_opt=paths=source_relative ./backend/event/*.proto

proto-build-calf-feed:
	protoc -I=. --go_out=:./proto/go/ --go_opt=paths=source_relative \
    --go-grpc_out=:./proto/go/ --go-grpc_opt=paths=source_relative ./backend/calf_feed/*.proto

proto-build-system:
	protoc -I=. --go_out=:./proto/go/ --go_opt=paths=source_relative \
    --go-grpc_out=:./proto/go/ --go-grpc_opt=paths=source_relative ./backend/system/*.proto

proto-build-cow:
	protoc -I=. --go_out=:./proto/go/ --go_opt=paths=source_relative \
        --go-grpc_out=:./proto/go/ --go-grpc_opt=paths=source_relative ./backend/cow/*.proto

proto-build-beef:
	protoc -I=. --go_out=:./proto/go/ --go_opt=paths=source_relative \
        --go-grpc_out=:./proto/go/ --go-grpc_opt=paths=source_relative ./backend/beef/*.proto