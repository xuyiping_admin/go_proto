syntax = "proto3";
package backend.operation;

option go_package = ".;operationPb";

import "backend/operation/enum.proto";
import "backend/operation/pagination.proto";
import "backend/operation/feed_formula.proto";

message AddPastureRequest {
  int32 id = 1;
  string name = 2; // 牧场名称
  string account = 3;   // 登录账号名称
  string manager_user = 4; // 牧场负责人名称
  string manager_phone = 5;   // 牧场负责人手机号
  string address = 6;   // 牧场地址
  IsShow.Kind is_show = 7;    // 是否启用
  int32 created_at = 8;    // 创建时间
  string created_at_format = 9;    // 创建时间格式化
  string domain = 10;    // 牧场端域名或者ip
  int32 pasture_id = 11;  // 牧场端id （兼容历史数据）
}

message SearchPastureRequest {
  string name = 1;       // 牧场名称
  string manager_user = 2; // 牧场负责人名称
  string manager_phone = 3;   // 牧场负责人手机号
  uint32 start_time = 4;       // 开始时间
  uint32 end_time = 5;    // 结束时间
  PaginationModel pagination = 6; // 分页
}

message SearchPastureResponse {
  int32 code = 1;
  string msg = 2;
  SearchPastureData data = 3;
}

message SearchPastureData {
  int32 page = 1;
  int32 total = 2;
  int32 page_size = 3;
  repeated AddPastureRequest list = 4;
}

message UserPasture {
  int32 id = 1;
  string name = 2;
}

message IsShowGroupPasture {
  int32 pasture_id = 1;    // 牧场id
  IsShow.Kind is_show = 2;     // 是否开启
}

// 添加牧畜分类
message AddCattleCategoryRequest {
  uint32 id = 1;
  CattleCategoryParent.Kind parent_id = 2;  // 父类id
  string parent_name = 3;        // 父类名称
  string name = 4;               // 牧畜分类名称
  string number = 5;            // 畜牧类别编号
  IsShow.Kind is_show = 6;      // 是否启用
  uint32 created_at = 7;         // 创建时间
  string created_at_format = 8;   // 创建时间格式
  string data_source = 9;         // 数据来源
}

// 是否启用
message IsShowCattleCategory {
  int32 cattle_category_id = 1;
  IsShow.Kind is_show = 2;
}

// 畜牧分类查询列表
message SearchCattleCategoryRequest {
  int32 parent_id = 1;
  IsShow.Kind is_show = 2;
  string name = 3;
  PaginationModel pagination = 4; // 分页
}

message SearchCattleCategoryResponse {
  int32 code = 1;
  string msg = 2;
  SearchCattleCategoryData data = 3;
}

message SearchCattleCategoryData {
  int32 page = 1;
  int32 total = 2;
  int32 page_size = 3;
  repeated AddCattleCategoryRequest list = 4;
}

// 添加饲料分类
message AddForageCategoryRequest {
  uint32 id = 1;
  ForageCategoryParent.Kind parent_id = 2;  // 父类id
  string parent_name = 3;        // 父类名称
  string name = 4;               // 牧畜分类名称
  string number = 5;            // 畜牧类别编号
  IsShow.Kind is_show = 6;      // 是否启用
  uint32 created_at = 7;         // 创建时间
  string created_at_format = 8;   // 创建时间格式
  string data_source = 9;         // 数据来源
}

// 是否启用
message IsShowForageCategory {
  int32 forage_category_id = 1;
  IsShow.Kind is_show = 2;
}

// 饲料分类查询列表
message SearchForageCategoryRequest {
  int32 parent_id = 1;
  IsShow.Kind is_show = 2;
  string name = 3;
  string number = 4;
  PaginationModel pagination = 5; // 分页
}

message SearchForageCategoryResponse {
  int32 code = 1;
  string msg = 2;
  SearchForageCategoryData data = 3;
}

message SearchForageCategoryData {
  int32 page = 1;
  int32 total = 2;
  int32 page_size = 3;
  repeated AddForageCategoryRequest list = 4;
}

// 饲料列表
message AddForageRequest {
  int32 id = 1;
  string name = 2;                      // 饲料名称
  int32 category_id = 3;                // 饲料分类id
  string category_name = 4;             // 饲料分类名称
  FormulaType.Kind material_type_key = 5;        // 物料类型key
  string material_type_name = 6;        // 物料类型名称
  string unique_encode = 7;              // 唯一编码
  ForageSource.Kind forage_source_id = 8;       // 饲料来源id
  string forage_source_name = 9;                // 饲料来源名称
  ForagePlanType.Kind plan_type_id = 10;       // 计划类型id
  string plan_type_name = 11;                  // 计划类型名称
  string  small_material_scale = 12;           // 小料称
  int32 allow_error = 13;                     // 允许误差 （单位kg）
  int32 package_weight = 14;                  // 包装重量 （单位kg）
  int32 price = 15;                          // 单价(单位分)
  int32 jump_weight = 16;                    // 跳转重量域（单位kg）
  JumpDelaType.Kind jump_delay = 17;         // 跳转延迟
  IsShow.Kind confirm_start = 18;            // 确认开始
  int32  relay_locations = 19;               // 继电器位置
  IsShow.Kind jmp = 20;                     // 无上域
  string backup1 = 21;                      // 备用字段1
  string backup2 = 22;                      // 备用字段2
  string backup3 = 23;                      // 备用字段3
  int32 created_at = 24;                    // 创建时间
  string created_at_format = 25;            // 创建时间格式化
  IsShow.Kind is_show = 26;                  // 是否启用
  string data_source = 27;                  // 数据来源
}

message SearchForageListRequest {
  string name = 1;              // 饲料名称
  int32 category_id = 2;        // 饲料分类id
  uint32 forage_source_id = 3;   // 饲料来源
  IsShow.Kind is_show = 4;       // 是否启用
  uint32 allow_error = 5;        // 允许误差
  uint32 jump_weight = 6;        // 跳转重量域
  JumpDelaType.Kind  jump_delay = 7;   // 跳转延迟
  PaginationModel pagination = 8;   // 分页
}

message ForageListSortRequest {
  repeated ForageListSort list = 1;
}

message ForageListSort {
  int64 id = 1;
  int64 sort = 2;
}

message SmallMaterialRequest {
  string api_name = 1;        // 牧场端接口标识名称
  int32 pasture_id = 2;       // 牧场id
  string info_name = 3;       //
}

message SearchForageListResponse {
 int32 code = 1;
 string msg = 2;
 SearchForageList data = 3;
}

message SearchForageList {
  int32 page = 1;
  int32 page_size = 2;
  int32 total = 3;
  repeated AddForageRequest list = 4;
}

// 是否启用
message IsShowForage {
  int32 forage_id = 1;
  IsShow.Kind is_show = 2;
}

message ForageEnumListResponse {
  int32 code = 1;
  string msg = 2;
  ForageEnumList data = 3;
}

message ForageEnumList {
  repeated ForageSourceEnum forage_source = 1;                    // 饲料来源
  repeated ForagePlanTypeEnum forage_plan_type = 2;               // 饲料计划类型
  repeated JumpDelaTypeEnum jump_dela_type = 3;                   // 跳转延迟类型
  repeated CattleParentCategoryEnum cattle_parent_category = 4;   // 畜牧分类
  repeated ForageParentCategoryEnum forage_parent_category = 5;   // 饲料分类
  repeated IsShowEnum is_show = 6;
  repeated FormulaTypeEnum formula_type = 7;                      // 配方类型
  repeated FormulaOptionEnum formula_list = 8;                    // 所有配方列表
  repeated IsShowEnum confirm_start = 9;                          // 确认开始
  repeated FormulaOptionEnum formulation_evaluation = 10;         // 配方评估查询方式
  repeated FormulaOptionEnum use_materials_list = 11;             // 用料分析-列表显示
  repeated FormulaOptionEnum use_materials_type = 12;             // 用料分析-统计类型
  repeated FormulaOptionEnum price_materials_type = 13;           // 价格分析-统计类型
  repeated FormulaOptionEnum jump_type = 14;                      // 准确性-跳转方式
  repeated FormulaOptionEnum statistics_type = 15;                // 准确性-统计类型
}

message ForageSourceEnum {
  ForageSource.Kind value = 1;
  string label = 2;
}

message ForagePlanTypeEnum {
  ForagePlanType.Kind value = 1;
  string label = 2;
}

message JumpDelaTypeEnum {
  JumpDelaType.Kind value = 1;
  string label = 2;
}

message CattleParentCategoryEnum {
  CattleCategoryParent.Kind value = 1;
  string label = 2;
}

message ForageParentCategoryEnum {
  ForageCategoryParent.Kind value = 1;
  string label = 2;
}

message IsShowEnum {
  IsShow.Kind value = 1;
  string label = 2;
}

message FormulaTypeEnum {
  FormulaType.Kind value = 1;
  string label = 2;
}

message FormulaOptionEnum {
  int32 value = 1;
  string label = 2;
}
// 牧场端分类数据同步
message CategorySyncRequest {
  string key_word = 1;         // 关键字
  int32 pasture_id = 2;        // 牧场id
  int32 parent_id = 3;         // 一类id
  string parent_name = 4;      // 一类名称
  string name = 5;             // 分类名称
  string number = 6;           // 分类编号
  IsShow.Kind is_show = 7;     // 是否展示
  IsShow.Kind is_delete = 8;   // 是否删除
  int32 id = 9;                // 牧场端数据id
}

// 牧场端分类数据删除
message CategoryDeleteRequest {
  string key_word = 1;    // 关键字
  int32 pasture_id = 2;   // 牧场id
  int32 data_id = 3;
}

message FeedFormulaSyncRequest {
  int32 pasture_id = 1;    // 牧场id
  int32 page = 2;          // 返回数据条数
  int32 page_size = 3;     // 返回数据条数
}

// 集团配方下发数据校验
message CheckDistributeData {
  repeated AddPastureRequest pasture_list = 1;    // 牧场列表
  repeated DistributeFeedRequest feed_formula_list = 2;
}
// 集团配方下发详情
message DistributeFeedRequest {
  int32 id = 1;
  string name = 2;                                       // 名称
  string  encode_number = 3;                             // 编码
  string colour = 4;                                     // 颜色
  CattleCategoryParent.Kind cattle_category_id = 5;      // 畜牧类别id
  string cattle_category_name = 6;                       // 畜牧类型名称
  FormulaType.Kind formula_type_id = 7;                   // 配方类型id
  string formula_type_name = 8;                           // 配方类型名称
  DataSource.Kind data_source_id = 9;                     // 数据来源
  string data_source_name = 10;                           // 数据来源
  string remarks = 11;                                    // 备注
  int32 version = 12;                                     // 版本号
  IsShow.Kind is_show = 13;                               // 是否启用
  IsShow.Kind is_modify = 14;                             // 是否可修改
  int32 created_at = 15;                                  // 创建时间
  string created_at_format = 16;                          // 创建时间格式化
  string pasture_name = 17;                               // 牧场名称
  int32 pasture_id = 18;                                  // 牧场id
  int32 pasture_data_id = 19;                             // 牧场端数据ID
  repeated AddFeedFormulaDetail feed_formula_detail = 20;   // 饲料详情列表

}

// 集团下发数据
message DistributeDataRequest {
  int32 pasture_id = 1;
  repeated DistributeFeedRequest feed_formula_list = 2;
}