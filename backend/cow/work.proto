syntax = "proto3";
package backend.cow;
option go_package = ".;cowPb";
import "backend/cow/enum.proto";

// SearchWorkOrderRequest 搜索工单请求
message SearchWorkOrderRequest {
  string name = 1;
  WorkOrderCategory.Kind categoryId = 2;
  WorkOrderFrequency.Kind frequency = 3;
  WorkOrderSubscribeUnit.Kind subscribeUnit = 4;
  Priority.Kind priority = 5;
  IsShow.Kind isShow = 6;
}

message SearchWorkOrderResponse {
  int32 code = 1;            // 是否成功
  string msg = 2;        // 消息
  SearchWorkOrderData data = 3;  // 数据
}

message SearchWorkOrderData {
  repeated WorkOrderList list = 1;  // 数据
  int32 total = 2;                      // 总条数
  int32 pageSize = 3;                  // 每页数量
  int32 page = 4;                       // 当前页数
}

message WorkOrderList {
  int32 id = 1;
  string name = 2;                         // 工单名称
  Priority.Kind priority = 3;              //  优先级
  string priorityName = 4;                 // 优先级名称
  string execTime = 5;                     // 执行时间
  WorkOrderCategory.Kind categoryId = 6;   // 工单类别
  string categoryName = 7;                 // 类别名称
  IsShow.Kind isShow = 8;                  // 是否显示
  WorkOrderFrequency.Kind frequency = 9;   // 执行频率
  string frequencyName = 10;               // 执行频率名称
  WorkOrderSubscribeUnit.Kind subscribeUnit = 11;       // 订阅单位
  string subscribeUnitName = 12;                        // 订阅单位名称
  repeated int32 execPersons = 13;                      // 执行人员
  repeated string execPersonsNames = 14;                // 执行人员名称
  repeated int32 execDepartmental = 15;                 // 执行部门
  repeated string execDepartmentalNames = 16;           // 执行部门名称
  string remarks = 17;                                  // 备注
  repeated string photos = 18;                          // 图集
  int32 operatorId = 19;                                // 操作人员
  string operatorName = 20;                             // 操作人员名称
  repeated int32 weekMonthValue = 21;                   // 星期值或者月份值
  repeated string weekMonthValueName = 22;              // 星期值或者月份值名称
  int32 createdAt = 25;
  int32 updatedAt = 26;
}

message SearchWorkOrderUserResponse {
  int32 code = 1;                    // 是否成功
  string msg = 2;                // 消息
  SearchWorkOrderUserData data = 3;  // 数据
}

message SearchWorkOrderUserData {
  repeated WorkOrderUserList list = 1;  // 数据
  int32 total = 2;                      // 总条数
  int32 pageSize = 3;                   // 每页数量
  int32 page = 4;                       // 当前页数
}

message WorkOrderUserList {
  string  workOrderNumber = 1;         // 工单编号
  string  workOrderName = 2;           // 工单名称
  string  workOrderCategoryName = 3;   // 工单类别名称
  string  workOrderPriorityName = 4;   // 工单优先级名称
  string  workOrderFrequencyName = 5;   // 工单执行频率名称
  string  workOrderExecTime = 6;        // 工单执行时间
  repeated string workOrderPhotos = 7;   // 工单图片集
  string  workOrderRemarks = 8;          // 工单备注
}

message UserWorkOrderResponse {
  int32 code = 1;              // 是否成功
  string msg = 2;          // 消息
  UserWorkOrderData data = 3;  // 数据
}

message UserWorkOrderData {
  repeated UserWorkOrderList list = 1;  // 数据
  int32 total = 2;                      // 总条数
  int32 pageSize = 3;                   // 每页数量
  int32 page = 4;                       // 当前页数
}

message UserWorkOrderList {
  string workOrderNumber = 1;  // 工单编号
  string workOrderName = 2;    // 工单名称
  int32 workOrderId = 3;
  string workOrderCategoryName = 4;   // 工单类别名称
  string workOrderPriorityName = 5;   // 工单优先级名称
  repeated string photos = 6;         // 工单图片集
  string remarks = 7;                 // 工单备注
  WorkOrderStatus.Kind status = 8;    // 工单状态
  int32 finishTime = 9;               // 工单完成时间
}

// 日历列表请求参数
message CalendarRequest {
  string showStartDay = 1;
  string showEndDay = 2;
}

// 日历列表返回参数
message CalendarResponse {
  int32 code = 1;                          // 是否成功
  string msg = 2;                          // 消息
  repeated Calendar data = 3;              // 数据
}

message ProgressList {
  CalendarType.Kind calendarTypeKind = 1;  // 日历类型
  string calendarName = 2;                 // 日历名称
  int32 incompleteTotal = 3;               // 当天未完成总数
  int32 completedCount = 4;                // 当天已完成数量
  string progress = 5;                     // 进度
}

message Calendar {
  int32 id = 1;
  string title = 2;                       // 名称
  CalendarType.Kind groupId = 3;          // 日历类型
  int32 count = 4;                        // 数量
  string start = 5;                       // 显示开始时间
  extendedProps extendedProps = 6;        // 扩展字段
  string color = 7;                       // 颜色
  string end = 8;                         // 显示结束时间
}

message extendedProps {
  string startDay = 1;
  string backup = 2;
}


// 日历列表请求参数
message CalendarTableRequest {
  CalendarType.Kind calendarType = 1;
  string start = 2;
}

// 工作台日历左侧待办列表请求参数
message CalendarToDoRequest {
  CalendarType.Kind calendarType = 1;      // 日历类型
  int32 cowId = 2;                         // 牛只Id
  int32 startAt = 3;                       // 开始时间
  int32 endAt = 4;                         // 结束时间
}

// 工作台日历左侧待办列表返回参数
message CalendarToDoResponse {
  int32 code = 1;             // 是否成功
  string msg = 2;         // 消息
  CalendarToDoData data = 3;  // 数据
}

message CalendarToDoData {
  repeated CalendarToDoList list = 1;      // 数据
  map <string, ProgressList> progress = 2;  // 进度列表
  int32 total = 3;                         // 总条数
  int32 pageSize = 4;                      // 每页数量
  int32 page = 5;                          // 当前页数
}

message CalendarToDoList {
  int32 id = 1;
  CalendarType.Kind calendarType = 2;         // 日历类型
  string calendarTypeName = 3;                // 日历类型名称
  string planDay = 4;                         // 任务日期
  string endDay = 5;                          // 结束日期
  string realityDay = 6;                      // 实际完成日期
  IsShow.Kind isFinish = 7;                   // 是否完成
  int32 operatorId = 8;                       // 操作人ID
  string operatorName = 9;                    // 操作人名称
  string remarks = 10;                        // 备注
  IsShow.Kind isExpire = 11;                  // 是否过期
  int32 cowId = 12;                           // 牛只ID
  string penName = 13;                        // 牛只名称
  int32 lact  = 14;                           // 胎次
  string earNumber = 15;                      // 耳号
  int32 remainingDays = 16;                   // 剩余天数
}

// 清单参数
message ItemsRequest {
  int32 cowId = 1;                      // 牛只ID
  int32 penId = 2;                      // 栏舍ID
  int32 lact = 3;                       // 胎次
  int32 immunizationId = 4;             // 免疫ID
  string startDay = 5;                  // 开始日期 YYYY-MM-DD
  string endDay = 6;                    // 结束日期 YYYY-MM-DD
  CalendarType.Kind calendarType = 7;   // 日历类型
  CowType.Kind cowType = 8;             // 牛只类型
  IsShow.Kind status = 9;               // 是否完成
  int32 pregnantCheckType = 10;         // 怀孕检查(1 初检 2 复检)
  int32 pastureId = 11;                 // 牧场id
  int32 sameTimeId = 12;                // 同期ID
  int32 sameTimeType = 13;              // 同期类型
  string earNumber = 14;                // 耳号
  int32 id = 15;                        // id
}

message ImmunizationItemsResponse {
  int32 code = 1;            // 是否成功
  string msg = 2;        // 消息
  ImmunizationItemsData data = 3;  // 数据
}

message ImmunizationItemsData {
  repeated ImmunizationItems list = 1;  // 数据
  map<string, string> header = 2;       // 标题
  int32 total = 3;                      // 总条数
  int32 pageSize = 4;                   // 每页数量
  int32 page = 5;                       // 当前页数
}

message ImmunizationItems {
  int32 id = 1;
  int32 cowId = 2;                      // 牛只ID
  int32 penId = 3;                      // 栏舍ID
  int32 lact = 4;                       // 胎次
  string penName = 5;                   // 栏舍名称
  int32 dayAge = 6;                     // 日龄
  string planDay = 7;                   // 计划免疫日期
  string realityDay = 8;                // 实际免疫日期
  IsShow.Kind status = 9;               // 是否完成
  string immunizationName = 10;         // 免疫名称
  int32 immunizationId  = 11;           // 免疫ID
  string remarks = 12;                  // 备注
  int32 operatorId = 13;                 // 操作人ID
  string operatorName = 14;              // 操作人名称
  int32 drugId = 15;                     // 药物ID
  string drugName = 16;                  // 药物名称
  Unit.Kind unit = 17;                   // 单位
  string unitName = 18;                  // 单位名称
  int32 usage = 19;                      // 使用量
  int32 createdAt = 20;                  // 创建时间
  int32 updatedAt = 21;                  // 更新时间
  string earNumber = 22;                // 耳号
}

message SameTimeItemResponse {
  int32 code = 1;            // 是否成功
  string msg = 2;        // 消息
  SameTimeItemsData data = 3;  // 数据
}

message SameTimeItemsData {
  repeated SameTimeItems list = 1;   // 数据
  map<string, string> header = 2;    // 标题
  int32 total = 3;                   // 总条数
  int32 pageSize = 4;                // 每页数量
  int32 page = 5;                    // 当前页数
}

message SameTimeItems {
  int32 id = 1;
  int32 cowId = 2;                      // 牛只ID
  int32 penId = 3;                      // 栏舍ID
  int32 lact = 4;                       // 胎次
  string penName = 5;                   // 栏舍名称
  int32 dayAge = 6;                     // 日龄
  string planDay = 7;                   // 同期日期
  int32 matingTimes = 8;                // 配种次数
  IsShow.Kind status = 10;              // 是否完成
  BreedStatus.Kind breedStatus = 11;    // 繁殖状态
  string breedStatusName = 12;          // 繁殖状态名称
  int32 calvingAge = 13;                // 产后天数
  int32 abortionAge = 14;               // 流产天数
  string calvingAtFormat = 15;          // 产犊日期
  string abortionAtFormat = 16;         // 流产日期
  string sameTimeTypeName = 17;         // 处理方式
  string cowTypeName = 18;              // 牛只类型名称
  string sameTimeName = 19;             // 同情名称
  float monthAge = 20;                  // 月龄
  string earNumber = 21;                // 耳号
}

// 孕检清单
message PregnancyCheckItemsResponse {
  int32 code = 1;                    // 是否成功
  string msg = 2;                // 消息
  PregnancyCheckItemsData data = 3;  // 数据
}

message PregnancyCheckItemsData {
  repeated PregnancyCheckItems list = 1;    // 数据
  map<string, string> header = 2;           // 标题
  int32 total = 3;                          // 总条数
  int32 pageSize = 4;                       // 每页数量
  int32 page = 5;                           // 当前页数
}

message PregnancyCheckItems {
  int32 id = 1;
  int32 cowId = 2;                      // 牛只ID
  int32 penId = 3;                      // 栏舍ID
  string penName = 4;                   // 栏舍名称
  int32 lact = 5;                       // 胎次
  string planDay = 6;                   // 计划检查日期
  IsShow.Kind status = 7;               // 是否完成
  string checkTypeName = 8;             // 检查名称（初检还是复检）
  string CowTypeName = 9;               // 牛只类型名称
  string breedStatusName = 10;          // 繁殖状态名称
  int32 matingTimes = 11;               // 配种次数
  string calvingAtFormat = 12;          // 产检日期
  string matingAtFormat = 13;           // 配种日期
  int32 matingAge = 14;                 // 配种后天数
  string bullId = 15;                   // 公牛号
  string earNumber = 16;                // 耳号
}

// 断奶清单
message WeaningItemsResponse {
  int32 code = 1;             // 是否成功
  string msg = 2;         // 消息
  WeaningItemsData data = 3;  // 数据
}

message WeaningItemsData {
  repeated WeaningItems list = 1;    // 数据
  map<string, string> header = 2;    // 标题
  int32 total = 3;                   // 总条数
  int32 pageSize = 4;                // 每页数量
  int32 page = 5;                    // 当前页数
}

message WeaningItems {
  int32 id = 1;              // 编号
  int32 cowId = 2;           // 牛只ID
  int32 penId = 3;           // 栏舍ID
  string penName = 4;        // 栏舍名称
  int32 dayAge = 5;          // 日龄
  string planDayFormat = 6;  // 计划断奶日期
  int32 motherId = 8;        // 母号
  string bullId = 9;         // 公牛号
  string earNumber = 10;     // 耳号
  string birthAtFormat = 11;  // 出生日期
  float currentWeight = 12;   // 当前体重
}

// 配种清单
message MatingItemsResponse {
  int32 code = 1;            // 是否成功
  string msg = 2;        // 消息
  MatingItemsData data = 3;  // 数据
}

message MatingItemsData {
  repeated MatingItems list = 1;     // 数据
  map<string, string> header = 2;    // 标题
  int32 total = 3;                   // 总条数
  int32 pageSize = 4;                // 每页数量
  int32 page = 5;                    // 当前页数
}

message MatingItems {
  int32 id = 1;
  int32 cowId = 2;             // 牛只ID
  int32 penId = 3;             // 栏舍ID
  string penName = 4;          // 栏舍名称
  int32 dayAge = 5;            // 日龄
  string planDay = 6;          // 计划配种日期
  IsShow.Kind status = 7;      // 是否完成
  string BreedStatusName = 8;  // 繁殖状态名称
  int32 matingTimes = 9;       // 配种次数
  int32 lact = 10;             // 胎次
  int32 calvingAge = 11;       // 产后天数
  int32 abortionAge = 12;      // 流产天数
  string bullId = 13;          // 公牛号
  string earNumber = 14;       // 耳号
  string exposeEstrusTypeName = 15;  // 发情揭发方式
  string LastCalvingAtFormat = 16;  // 最近一次产犊日期
}

// 产犊清单
message CalvingItemsResponse {
  int32 code = 1;             // 是否成功
  string msg = 2;         // 消息
  CalvingItemsData data = 3;  // 数据
}

message CalvingItemsData {
  repeated CalvingItems list = 1;    // 数据
  map<string, string> header = 2;   // 标题
  int32 total = 3;                 // 总条数
  int32 pageSize = 4;              // 每页数量
  int32 page = 5;                 // 当前页数
}

message CalvingItems {
  int32 id = 1;
  int32 cowId = 2;             // 牛只ID
  int32 penId = 3;             // 栏舍ID
  string penName = 4;          // 栏舍名称
  int32 dayAge = 5;            // 日龄
  string planDay = 6;          // 产犊日期
  IsShow.Kind status = 7;      // 是否完成
  string BreedStatusName = 8;  // 繁殖状态名称
  int32 matingTimes = 9;       // 配种次数
  int32 lact = 10;             // 胎次
  string bullId = 11;          // 公牛号
  string matingAtFormat = 12;  // 配种日期
  int32 matingAge = 13;        // 配种后天数
  BreedStatus.Kind breedStatus = 14;  // 繁殖状态
  string earNumber = 15;       // 耳号
  float weight = 16;           // 体重
}

// 发情清单
message EstrusItemsRequest {
  repeated int32 cowIds = 1;   // 牛只ID
  repeated int32 penIds = 2;   // 牛舍ID
  EstrusLevel.Kind level = 3;  // 发情等级
  string earNumber = 4;      // 耳号
  int32 startDayAt = 5;      // 开始日期
  int32 endDayAt = 6;        // 结束日期
  repeated string earNumbers = 7;    // 耳号集合
}

message EventEstrusResponse {
  int32 code = 1;            // 是否成功
  string msg = 2;        // 消息
  EstrusItemsData data = 3;  // 数据
}

message EstrusItemsData {
  repeated EstrusItems list = 1;    // 数据
  int32 total = 2;                // 总条数
  int32 pageSize = 3;              // 每页数量
  int32 page = 4;                // 当前页数
}

message EstrusItems {
  int32 id = 1;
  int32 cowId = 2;                      // 牛只ID
  int32 penId = 3;                      // 栏舍ID
  string penName = 4;                   // 栏舍名称
  int32 dayAge = 5;                     // 日龄
  string planDay = 6;                   // 发情日期
  IsShow.Kind status = 7;               // 是否完成
  string breedStatusName = 8;           // 繁殖状态名称
  int32 matingTimes = 9;                // 配种次数
  int32 lact = 10;                      // 胎次
  int32 calvingAge = 11;                // 产后天数
  int32 abortionAge = 12;               // 流产天数
  string optimumMatingStartTime  = 13;  // 最佳配种时间
  string optimumMatingEndTime  = 14;    // 最佳配种时间
  string lastBreedEventDetails = 15;    // 最近繁殖事件详情
  EstrusLevel.Kind level = 16;          // 发情等级
  int32 estrusInterval = 17;            // 发情间隔
  string earNumber = 18;                // 耳号
  string bestMatingTime = 19;           // 最佳配种时间
  int32 pastureId = 20;                 // 牧场Id
  string estrusStartTime = 21;          // 发情开始时间
  string estrusEndTime = 22;            // 发情结束时间
}