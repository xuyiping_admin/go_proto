#!/bin/bash
set -e
### proto build
echo "============ go proto start ================"
make proto-build
make proto-build-xdmy
make proto-build-calf-feed
make proto-build-event
make proto-build-system
make proto-build-cow
make proto-build-beef
echo "============  go proto end  ================"