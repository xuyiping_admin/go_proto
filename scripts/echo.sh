#!/bin/bash
set -euo pipefail
trap "echo 'error: Script failed: see failed command above'" ERR

# init GO path
ln -s /usr/local/go/bin/go /bin/go
mkdir -p $HOME/go && mkdir -p $HOME/src && mkdir -p $HOME/bin && mkdir -p $HOME/pkg
export GOPATH=$HOME/go

# clone repo
mkdir -p $GOPATH/src/git.llsapp.com/pb/common-go
cp -R . $GOPATH/src/git.llsapp.com/pb/common-go

# 替换 import path
echo "import..."
go run main.go

# commit
git add .
git commit -m "$proto_tag" --allow-empty
latest_tag=$(git describe --tags `git rev-list --tags --max-count=1`)
new_tag=$(echo $latest_tag | awk -F. -v OFS=. '{++$NF;print};')
git tag -a "$new_tag" -m "$proto_tag"
git push -u origin bot-build-local:bot-build --tags -f
echo -e "\033[0;32m又更新 PB 了，你真棒!\033[0m"