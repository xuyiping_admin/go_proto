module gitee.com/xuyiping_admin/go_proto

go 1.17

require google.golang.org/protobuf v1.34.1

require (
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.20.0 // indirect
	golang.org/x/text v0.15.0 // indirect
	google.golang.org/genproto/googleapis/api v0.0.0-20240513163218-0867130af1f8 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240513163218-0867130af1f8 // indirect
	google.golang.org/grpc v1.64.0 // indirect
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.3.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
