// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.1
// 	protoc        v5.26.1
// source: backend/xdmy/request.proto

package modernPb

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type SparePartsRequisitionsRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	PastureId      int32             `protobuf:"varint,1,opt,name=pastureId,proto3" json:"pastureId,omitempty"`                                             // 牧场id
	DepartmentId   int32             `protobuf:"varint,2,opt,name=departmentId,proto3" json:"departmentId,omitempty"`                                       // 部门id
	OrderNumber    string            `protobuf:"bytes,3,opt,name=orderNumber,proto3" json:"orderNumber,omitempty"`                                          // 申购单号
	CreateTime     string            `protobuf:"bytes,4,opt,name=createTime,proto3" json:"createTime,omitempty"`                                            // 申购日期
	EmployeId      int32             `protobuf:"varint,5,opt,name=employeId,proto3" json:"employeId,omitempty"`                                             // 员工id
	ProviderId     int32             `protobuf:"varint,6,opt,name=providerId,proto3" json:"providerId,omitempty"`                                           // 供应商编号
	FunderId       int32             `protobuf:"varint,7,opt,name=funderId,proto3" json:"funderId,omitempty"`                                               // 垫资人
	PurchaseType   PurchaseType_Kind `protobuf:"varint,8,opt,name=purchaseType,proto3,enum=backend.modern.PurchaseType_Kind" json:"purchaseType,omitempty"` // 申购类型
	SpotList       []*SpotList       `protobuf:"bytes,9,rep,name=spot_list,json=spotList,proto3" json:"spot_list,omitempty"`
	ProviderName   string            `protobuf:"bytes,10,opt,name=providerName,proto3" json:"providerName,omitempty"`     // 供应商名称
	DepartmentName string            `protobuf:"bytes,11,opt,name=departmentName,proto3" json:"departmentName,omitempty"` // 部门名称
}

func (x *SparePartsRequisitionsRequest) Reset() {
	*x = SparePartsRequisitionsRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_backend_xdmy_request_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SparePartsRequisitionsRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SparePartsRequisitionsRequest) ProtoMessage() {}

func (x *SparePartsRequisitionsRequest) ProtoReflect() protoreflect.Message {
	mi := &file_backend_xdmy_request_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SparePartsRequisitionsRequest.ProtoReflect.Descriptor instead.
func (*SparePartsRequisitionsRequest) Descriptor() ([]byte, []int) {
	return file_backend_xdmy_request_proto_rawDescGZIP(), []int{0}
}

func (x *SparePartsRequisitionsRequest) GetPastureId() int32 {
	if x != nil {
		return x.PastureId
	}
	return 0
}

func (x *SparePartsRequisitionsRequest) GetDepartmentId() int32 {
	if x != nil {
		return x.DepartmentId
	}
	return 0
}

func (x *SparePartsRequisitionsRequest) GetOrderNumber() string {
	if x != nil {
		return x.OrderNumber
	}
	return ""
}

func (x *SparePartsRequisitionsRequest) GetCreateTime() string {
	if x != nil {
		return x.CreateTime
	}
	return ""
}

func (x *SparePartsRequisitionsRequest) GetEmployeId() int32 {
	if x != nil {
		return x.EmployeId
	}
	return 0
}

func (x *SparePartsRequisitionsRequest) GetProviderId() int32 {
	if x != nil {
		return x.ProviderId
	}
	return 0
}

func (x *SparePartsRequisitionsRequest) GetFunderId() int32 {
	if x != nil {
		return x.FunderId
	}
	return 0
}

func (x *SparePartsRequisitionsRequest) GetPurchaseType() PurchaseType_Kind {
	if x != nil {
		return x.PurchaseType
	}
	return PurchaseType_Invalid
}

func (x *SparePartsRequisitionsRequest) GetSpotList() []*SpotList {
	if x != nil {
		return x.SpotList
	}
	return nil
}

func (x *SparePartsRequisitionsRequest) GetProviderName() string {
	if x != nil {
		return x.ProviderName
	}
	return ""
}

func (x *SparePartsRequisitionsRequest) GetDepartmentName() string {
	if x != nil {
		return x.DepartmentName
	}
	return ""
}

type SpotList struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	PartId        int64   `protobuf:"varint,2,opt,name=partId,proto3" json:"partId,omitempty"`              // 备件关联Id
	PartCode      string  `protobuf:"bytes,3,opt,name=partCode,proto3" json:"partCode,omitempty"`           // 备件编码
	PartName      string  `protobuf:"bytes,4,opt,name=partName,proto3" json:"partName,omitempty"`           // 物料编号
	Specification string  `protobuf:"bytes,5,opt,name=specification,proto3" json:"specification,omitempty"` // 规格
	Unit          string  `protobuf:"bytes,6,opt,name=unit,proto3" json:"unit,omitempty"`                   // 单位
	BrandId       int32   `protobuf:"varint,7,opt,name=brandId,proto3" json:"brandId,omitempty"`
	StorageAmount float32 `protobuf:"fixed32,8,opt,name=storageAmount,proto3" json:"storageAmount,omitempty"` // 申购时库存数量
	Purpose       string  `protobuf:"bytes,9,opt,name=purpose,proto3" json:"purpose,omitempty"`               // 用途
	Amount        string  `protobuf:"bytes,10,opt,name=amount,proto3" json:"amount,omitempty"`                // 申购数量
	Price         string  `protobuf:"bytes,11,opt,name=price,proto3" json:"price,omitempty"`
	ContractId    string  `protobuf:"bytes,12,opt,name=contractId,proto3" json:"contractId,omitempty"` // 合同id
	DepartmentId  string  `protobuf:"bytes,13,opt,name=departmentId,proto3" json:"departmentId,omitempty"`
	Date          string  `protobuf:"bytes,14,opt,name=date,proto3" json:"date,omitempty"`
	EmployeId     string  `protobuf:"bytes,15,opt,name=employeId,proto3" json:"employeId,omitempty"`
}

func (x *SpotList) Reset() {
	*x = SpotList{}
	if protoimpl.UnsafeEnabled {
		mi := &file_backend_xdmy_request_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SpotList) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SpotList) ProtoMessage() {}

func (x *SpotList) ProtoReflect() protoreflect.Message {
	mi := &file_backend_xdmy_request_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SpotList.ProtoReflect.Descriptor instead.
func (*SpotList) Descriptor() ([]byte, []int) {
	return file_backend_xdmy_request_proto_rawDescGZIP(), []int{1}
}

func (x *SpotList) GetPartId() int64 {
	if x != nil {
		return x.PartId
	}
	return 0
}

func (x *SpotList) GetPartCode() string {
	if x != nil {
		return x.PartCode
	}
	return ""
}

func (x *SpotList) GetPartName() string {
	if x != nil {
		return x.PartName
	}
	return ""
}

func (x *SpotList) GetSpecification() string {
	if x != nil {
		return x.Specification
	}
	return ""
}

func (x *SpotList) GetUnit() string {
	if x != nil {
		return x.Unit
	}
	return ""
}

func (x *SpotList) GetBrandId() int32 {
	if x != nil {
		return x.BrandId
	}
	return 0
}

func (x *SpotList) GetStorageAmount() float32 {
	if x != nil {
		return x.StorageAmount
	}
	return 0
}

func (x *SpotList) GetPurpose() string {
	if x != nil {
		return x.Purpose
	}
	return ""
}

func (x *SpotList) GetAmount() string {
	if x != nil {
		return x.Amount
	}
	return ""
}

func (x *SpotList) GetPrice() string {
	if x != nil {
		return x.Price
	}
	return ""
}

func (x *SpotList) GetContractId() string {
	if x != nil {
		return x.ContractId
	}
	return ""
}

func (x *SpotList) GetDepartmentId() string {
	if x != nil {
		return x.DepartmentId
	}
	return ""
}

func (x *SpotList) GetDate() string {
	if x != nil {
		return x.Date
	}
	return ""
}

func (x *SpotList) GetEmployeId() string {
	if x != nil {
		return x.EmployeId
	}
	return ""
}

// 柴油类型列表
type DieselTypeListResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code int32           `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	Msg  string          `protobuf:"bytes,2,opt,name=msg,proto3" json:"msg,omitempty"`
	Data *DieselTypeList `protobuf:"bytes,3,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *DieselTypeListResponse) Reset() {
	*x = DieselTypeListResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_backend_xdmy_request_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DieselTypeListResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DieselTypeListResponse) ProtoMessage() {}

func (x *DieselTypeListResponse) ProtoReflect() protoreflect.Message {
	mi := &file_backend_xdmy_request_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DieselTypeListResponse.ProtoReflect.Descriptor instead.
func (*DieselTypeListResponse) Descriptor() ([]byte, []int) {
	return file_backend_xdmy_request_proto_rawDescGZIP(), []int{2}
}

func (x *DieselTypeListResponse) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *DieselTypeListResponse) GetMsg() string {
	if x != nil {
		return x.Msg
	}
	return ""
}

func (x *DieselTypeListResponse) GetData() *DieselTypeList {
	if x != nil {
		return x.Data
	}
	return nil
}

type DieselTypeList struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	DieselType []*DieselTypeEnum `protobuf:"bytes,1,rep,name=diesel_type,json=dieselType,proto3" json:"diesel_type,omitempty"` // 柴油类型
}

func (x *DieselTypeList) Reset() {
	*x = DieselTypeList{}
	if protoimpl.UnsafeEnabled {
		mi := &file_backend_xdmy_request_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DieselTypeList) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DieselTypeList) ProtoMessage() {}

func (x *DieselTypeList) ProtoReflect() protoreflect.Message {
	mi := &file_backend_xdmy_request_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DieselTypeList.ProtoReflect.Descriptor instead.
func (*DieselTypeList) Descriptor() ([]byte, []int) {
	return file_backend_xdmy_request_proto_rawDescGZIP(), []int{3}
}

func (x *DieselTypeList) GetDieselType() []*DieselTypeEnum {
	if x != nil {
		return x.DieselType
	}
	return nil
}

type DieselTypeEnum struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Value DieselType_Kind `protobuf:"varint,1,opt,name=value,proto3,enum=backend.modern.DieselType_Kind" json:"value,omitempty"`
	Label string          `protobuf:"bytes,2,opt,name=label,proto3" json:"label,omitempty"`
}

func (x *DieselTypeEnum) Reset() {
	*x = DieselTypeEnum{}
	if protoimpl.UnsafeEnabled {
		mi := &file_backend_xdmy_request_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DieselTypeEnum) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DieselTypeEnum) ProtoMessage() {}

func (x *DieselTypeEnum) ProtoReflect() protoreflect.Message {
	mi := &file_backend_xdmy_request_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DieselTypeEnum.ProtoReflect.Descriptor instead.
func (*DieselTypeEnum) Descriptor() ([]byte, []int) {
	return file_backend_xdmy_request_proto_rawDescGZIP(), []int{4}
}

func (x *DieselTypeEnum) GetValue() DieselType_Kind {
	if x != nil {
		return x.Value
	}
	return DieselType_Invalid
}

func (x *DieselTypeEnum) GetLabel() string {
	if x != nil {
		return x.Label
	}
	return ""
}

var File_backend_xdmy_request_proto protoreflect.FileDescriptor

var file_backend_xdmy_request_proto_rawDesc = []byte{
	0x0a, 0x1a, 0x62, 0x61, 0x63, 0x6b, 0x65, 0x6e, 0x64, 0x2f, 0x78, 0x64, 0x6d, 0x79, 0x2f, 0x72,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0e, 0x62, 0x61,
	0x63, 0x6b, 0x65, 0x6e, 0x64, 0x2e, 0x6d, 0x6f, 0x64, 0x65, 0x72, 0x6e, 0x1a, 0x17, 0x62, 0x61,
	0x63, 0x6b, 0x65, 0x6e, 0x64, 0x2f, 0x78, 0x64, 0x6d, 0x79, 0x2f, 0x65, 0x6e, 0x75, 0x6d, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xc7, 0x03, 0x0a, 0x1d, 0x53, 0x70, 0x61, 0x72, 0x65, 0x50,
	0x61, 0x72, 0x74, 0x73, 0x52, 0x65, 0x71, 0x75, 0x69, 0x73, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x73,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1c, 0x0a, 0x09, 0x70, 0x61, 0x73, 0x74, 0x75,
	0x72, 0x65, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x09, 0x70, 0x61, 0x73, 0x74,
	0x75, 0x72, 0x65, 0x49, 0x64, 0x12, 0x22, 0x0a, 0x0c, 0x64, 0x65, 0x70, 0x61, 0x72, 0x74, 0x6d,
	0x65, 0x6e, 0x74, 0x49, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x05, 0x52, 0x0c, 0x64, 0x65, 0x70,
	0x61, 0x72, 0x74, 0x6d, 0x65, 0x6e, 0x74, 0x49, 0x64, 0x12, 0x20, 0x0a, 0x0b, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x12, 0x1e, 0x0a, 0x0a, 0x63,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x12, 0x1c, 0x0a, 0x09, 0x65,
	0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x49, 0x64, 0x18, 0x05, 0x20, 0x01, 0x28, 0x05, 0x52, 0x09,
	0x65, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x49, 0x64, 0x12, 0x1e, 0x0a, 0x0a, 0x70, 0x72, 0x6f,
	0x76, 0x69, 0x64, 0x65, 0x72, 0x49, 0x64, 0x18, 0x06, 0x20, 0x01, 0x28, 0x05, 0x52, 0x0a, 0x70,
	0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x49, 0x64, 0x12, 0x1a, 0x0a, 0x08, 0x66, 0x75, 0x6e,
	0x64, 0x65, 0x72, 0x49, 0x64, 0x18, 0x07, 0x20, 0x01, 0x28, 0x05, 0x52, 0x08, 0x66, 0x75, 0x6e,
	0x64, 0x65, 0x72, 0x49, 0x64, 0x12, 0x45, 0x0a, 0x0c, 0x70, 0x75, 0x72, 0x63, 0x68, 0x61, 0x73,
	0x65, 0x54, 0x79, 0x70, 0x65, 0x18, 0x08, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x21, 0x2e, 0x62, 0x61,
	0x63, 0x6b, 0x65, 0x6e, 0x64, 0x2e, 0x6d, 0x6f, 0x64, 0x65, 0x72, 0x6e, 0x2e, 0x50, 0x75, 0x72,
	0x63, 0x68, 0x61, 0x73, 0x65, 0x54, 0x79, 0x70, 0x65, 0x2e, 0x4b, 0x69, 0x6e, 0x64, 0x52, 0x0c,
	0x70, 0x75, 0x72, 0x63, 0x68, 0x61, 0x73, 0x65, 0x54, 0x79, 0x70, 0x65, 0x12, 0x35, 0x0a, 0x09,
	0x73, 0x70, 0x6f, 0x74, 0x5f, 0x6c, 0x69, 0x73, 0x74, 0x18, 0x09, 0x20, 0x03, 0x28, 0x0b, 0x32,
	0x18, 0x2e, 0x62, 0x61, 0x63, 0x6b, 0x65, 0x6e, 0x64, 0x2e, 0x6d, 0x6f, 0x64, 0x65, 0x72, 0x6e,
	0x2e, 0x53, 0x70, 0x6f, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x08, 0x73, 0x70, 0x6f, 0x74, 0x4c,
	0x69, 0x73, 0x74, 0x12, 0x22, 0x0a, 0x0c, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x4e,
	0x61, 0x6d, 0x65, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x70, 0x72, 0x6f, 0x76, 0x69,
	0x64, 0x65, 0x72, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x26, 0x0a, 0x0e, 0x64, 0x65, 0x70, 0x61, 0x72,
	0x74, 0x6d, 0x65, 0x6e, 0x74, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0e, 0x64, 0x65, 0x70, 0x61, 0x72, 0x74, 0x6d, 0x65, 0x6e, 0x74, 0x4e, 0x61, 0x6d, 0x65, 0x22,
	0x92, 0x03, 0x0a, 0x08, 0x53, 0x70, 0x6f, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06,
	0x70, 0x61, 0x72, 0x74, 0x49, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x70, 0x61,
	0x72, 0x74, 0x49, 0x64, 0x12, 0x1a, 0x0a, 0x08, 0x70, 0x61, 0x72, 0x74, 0x43, 0x6f, 0x64, 0x65,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x70, 0x61, 0x72, 0x74, 0x43, 0x6f, 0x64, 0x65,
	0x12, 0x1a, 0x0a, 0x08, 0x70, 0x61, 0x72, 0x74, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x08, 0x70, 0x61, 0x72, 0x74, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x24, 0x0a, 0x0d,
	0x73, 0x70, 0x65, 0x63, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x05, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x0d, 0x73, 0x70, 0x65, 0x63, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x12, 0x12, 0x0a, 0x04, 0x75, 0x6e, 0x69, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x04, 0x75, 0x6e, 0x69, 0x74, 0x12, 0x18, 0x0a, 0x07, 0x62, 0x72, 0x61, 0x6e, 0x64, 0x49,
	0x64, 0x18, 0x07, 0x20, 0x01, 0x28, 0x05, 0x52, 0x07, 0x62, 0x72, 0x61, 0x6e, 0x64, 0x49, 0x64,
	0x12, 0x24, 0x0a, 0x0d, 0x73, 0x74, 0x6f, 0x72, 0x61, 0x67, 0x65, 0x41, 0x6d, 0x6f, 0x75, 0x6e,
	0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x02, 0x52, 0x0d, 0x73, 0x74, 0x6f, 0x72, 0x61, 0x67, 0x65,
	0x41, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x18, 0x0a, 0x07, 0x70, 0x75, 0x72, 0x70, 0x6f, 0x73,
	0x65, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x70, 0x75, 0x72, 0x70, 0x6f, 0x73, 0x65,
	0x12, 0x16, 0x0a, 0x06, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x06, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x72, 0x69, 0x63,
	0x65, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x70, 0x72, 0x69, 0x63, 0x65, 0x12, 0x1e,
	0x0a, 0x0a, 0x63, 0x6f, 0x6e, 0x74, 0x72, 0x61, 0x63, 0x74, 0x49, 0x64, 0x18, 0x0c, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x0a, 0x63, 0x6f, 0x6e, 0x74, 0x72, 0x61, 0x63, 0x74, 0x49, 0x64, 0x12, 0x22,
	0x0a, 0x0c, 0x64, 0x65, 0x70, 0x61, 0x72, 0x74, 0x6d, 0x65, 0x6e, 0x74, 0x49, 0x64, 0x18, 0x0d,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x64, 0x65, 0x70, 0x61, 0x72, 0x74, 0x6d, 0x65, 0x6e, 0x74,
	0x49, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x65, 0x18, 0x0e, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x04, 0x64, 0x61, 0x74, 0x65, 0x12, 0x1c, 0x0a, 0x09, 0x65, 0x6d, 0x70, 0x6c, 0x6f, 0x79,
	0x65, 0x49, 0x64, 0x18, 0x0f, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x65, 0x6d, 0x70, 0x6c, 0x6f,
	0x79, 0x65, 0x49, 0x64, 0x22, 0x72, 0x0a, 0x16, 0x44, 0x69, 0x65, 0x73, 0x65, 0x6c, 0x54, 0x79,
	0x70, 0x65, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x12,
	0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x63, 0x6f,
	0x64, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x6d, 0x73, 0x67, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x03, 0x6d, 0x73, 0x67, 0x12, 0x32, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x1e, 0x2e, 0x62, 0x61, 0x63, 0x6b, 0x65, 0x6e, 0x64, 0x2e, 0x6d, 0x6f, 0x64,
	0x65, 0x72, 0x6e, 0x2e, 0x44, 0x69, 0x65, 0x73, 0x65, 0x6c, 0x54, 0x79, 0x70, 0x65, 0x4c, 0x69,
	0x73, 0x74, 0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x22, 0x51, 0x0a, 0x0e, 0x44, 0x69, 0x65, 0x73,
	0x65, 0x6c, 0x54, 0x79, 0x70, 0x65, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x3f, 0x0a, 0x0b, 0x64, 0x69,
	0x65, 0x73, 0x65, 0x6c, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32,
	0x1e, 0x2e, 0x62, 0x61, 0x63, 0x6b, 0x65, 0x6e, 0x64, 0x2e, 0x6d, 0x6f, 0x64, 0x65, 0x72, 0x6e,
	0x2e, 0x44, 0x69, 0x65, 0x73, 0x65, 0x6c, 0x54, 0x79, 0x70, 0x65, 0x45, 0x6e, 0x75, 0x6d, 0x52,
	0x0a, 0x64, 0x69, 0x65, 0x73, 0x65, 0x6c, 0x54, 0x79, 0x70, 0x65, 0x22, 0x5d, 0x0a, 0x0e, 0x44,
	0x69, 0x65, 0x73, 0x65, 0x6c, 0x54, 0x79, 0x70, 0x65, 0x45, 0x6e, 0x75, 0x6d, 0x12, 0x35, 0x0a,
	0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x1f, 0x2e, 0x62,
	0x61, 0x63, 0x6b, 0x65, 0x6e, 0x64, 0x2e, 0x6d, 0x6f, 0x64, 0x65, 0x72, 0x6e, 0x2e, 0x44, 0x69,
	0x65, 0x73, 0x65, 0x6c, 0x54, 0x79, 0x70, 0x65, 0x2e, 0x4b, 0x69, 0x6e, 0x64, 0x52, 0x05, 0x76,
	0x61, 0x6c, 0x75, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x61, 0x62, 0x65, 0x6c, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x05, 0x6c, 0x61, 0x62, 0x65, 0x6c, 0x42, 0x0c, 0x5a, 0x0a, 0x2e, 0x3b,
	0x6d, 0x6f, 0x64, 0x65, 0x72, 0x6e, 0x50, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_backend_xdmy_request_proto_rawDescOnce sync.Once
	file_backend_xdmy_request_proto_rawDescData = file_backend_xdmy_request_proto_rawDesc
)

func file_backend_xdmy_request_proto_rawDescGZIP() []byte {
	file_backend_xdmy_request_proto_rawDescOnce.Do(func() {
		file_backend_xdmy_request_proto_rawDescData = protoimpl.X.CompressGZIP(file_backend_xdmy_request_proto_rawDescData)
	})
	return file_backend_xdmy_request_proto_rawDescData
}

var file_backend_xdmy_request_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_backend_xdmy_request_proto_goTypes = []interface{}{
	(*SparePartsRequisitionsRequest)(nil), // 0: backend.modern.SparePartsRequisitionsRequest
	(*SpotList)(nil),                      // 1: backend.modern.SpotList
	(*DieselTypeListResponse)(nil),        // 2: backend.modern.DieselTypeListResponse
	(*DieselTypeList)(nil),                // 3: backend.modern.DieselTypeList
	(*DieselTypeEnum)(nil),                // 4: backend.modern.DieselTypeEnum
	(PurchaseType_Kind)(0),                // 5: backend.modern.PurchaseType.Kind
	(DieselType_Kind)(0),                  // 6: backend.modern.DieselType.Kind
}
var file_backend_xdmy_request_proto_depIdxs = []int32{
	5, // 0: backend.modern.SparePartsRequisitionsRequest.purchaseType:type_name -> backend.modern.PurchaseType.Kind
	1, // 1: backend.modern.SparePartsRequisitionsRequest.spot_list:type_name -> backend.modern.SpotList
	3, // 2: backend.modern.DieselTypeListResponse.data:type_name -> backend.modern.DieselTypeList
	4, // 3: backend.modern.DieselTypeList.diesel_type:type_name -> backend.modern.DieselTypeEnum
	6, // 4: backend.modern.DieselTypeEnum.value:type_name -> backend.modern.DieselType.Kind
	5, // [5:5] is the sub-list for method output_type
	5, // [5:5] is the sub-list for method input_type
	5, // [5:5] is the sub-list for extension type_name
	5, // [5:5] is the sub-list for extension extendee
	0, // [0:5] is the sub-list for field type_name
}

func init() { file_backend_xdmy_request_proto_init() }
func file_backend_xdmy_request_proto_init() {
	if File_backend_xdmy_request_proto != nil {
		return
	}
	file_backend_xdmy_enum_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_backend_xdmy_request_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SparePartsRequisitionsRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_backend_xdmy_request_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SpotList); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_backend_xdmy_request_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DieselTypeListResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_backend_xdmy_request_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DieselTypeList); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_backend_xdmy_request_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DieselTypeEnum); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_backend_xdmy_request_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_backend_xdmy_request_proto_goTypes,
		DependencyIndexes: file_backend_xdmy_request_proto_depIdxs,
		MessageInfos:      file_backend_xdmy_request_proto_msgTypes,
	}.Build()
	File_backend_xdmy_request_proto = out.File
	file_backend_xdmy_request_proto_rawDesc = nil
	file_backend_xdmy_request_proto_goTypes = nil
	file_backend_xdmy_request_proto_depIdxs = nil
}
