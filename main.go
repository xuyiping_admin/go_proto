package main

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	output("", replace("./go_proto", "git.llsapp.com/common/protos", "git.llsapp.com/pb/common-go"))
}

func replace(rootDir, old, new string) error {
	return filepath.Walk(rootDir, func(path string, f os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if f.IsDir() {
			return nil
		}

		matched, err := filepath.Match("*.pb.go", f.Name())
		if err != nil {
			return err
		}
		if matched {
			read, err := ioutil.ReadFile(path)
			if err != nil {
				return err
			}
			newContents := strings.Replace(string(read), old, new, -1)

			err = ioutil.WriteFile(path, []byte(newContents), 0)
			if err != nil {
				return err
			}
		}
		return nil
	})
}

func output(action string, err error) {
	if err != nil {
		panic(err)
	}
}
